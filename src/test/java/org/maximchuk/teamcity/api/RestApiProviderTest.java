package org.maximchuk.teamcity.api;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.maximchuk.teamcity.api.entry.BuildEntry;
import org.maximchuk.teamcity.api.entry.BuildTypeEntry;
import org.maximchuk.teamcity.api.entry.ProjectEntry;
import org.maximchuk.teamcity.api.provider.impl.RestApiProvider;

import java.util.List;

/**
 * @author Maxim L. Maximcuhk
 *         Date: 07.01.14
 */
public class RestApiProviderTest {

    private static final String SERVERNAME = "servername";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";



    private RestApiProvider apiProvider;

    @Before
    public void setUp() throws Exception {
        apiProvider = new RestApiProvider(SERVERNAME, USERNAME, PASSWORD, new Base64EncoderImpl());
    }

    @Test
    public void testGetProjects() throws Exception {
        List<ProjectEntry> projects = apiProvider.getProjects();
        Assert.assertFalse(projects.isEmpty());
    }

    @Test
    public void testGetBuildTypes() throws Exception {
        List<ProjectEntry> projects = apiProvider.getProjects();
        List<BuildTypeEntry> buildTypes = apiProvider.getBuildTypes(projects.get(1));

        Assert.assertFalse(buildTypes.isEmpty());

    }

    @Test
    public void testGetBuilds() throws Exception {
        List<ProjectEntry> projects = apiProvider.getProjects();
        List<BuildTypeEntry> buildTypes = apiProvider.getBuildTypes(projects.get(1));
        List<BuildEntry> builds = apiProvider.getBuilds(buildTypes.get(0));

        Assert.assertFalse(builds.isEmpty());
    }

    @Test
    public void testCheckConnection() throws Exception {
        Assert.assertTrue(apiProvider.checkConnection());
    }

}
