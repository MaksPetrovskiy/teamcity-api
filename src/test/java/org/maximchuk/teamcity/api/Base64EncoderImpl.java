package org.maximchuk.teamcity.api;

import org.maximchuk.teamcity.api.provider.IBase64Encoder;
import sun.misc.BASE64Encoder;

/**
 * @author Maxim L. Maximcuhk
 *         Date: 08.01.14
 */
public class Base64EncoderImpl implements IBase64Encoder {

    @Override
    public String encode(byte[] data) {
        return new BASE64Encoder().encode(data);
    }

}
