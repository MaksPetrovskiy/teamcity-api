package org.maximchuk.teamcity.api.provider;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.maximchuk.teamcity.api.exception.HttpException;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Maxim L. Maximcuhk
 *         Date: 07.01.14
 */
public abstract class AbstractApiProvider {

    public static final int DEFAULT_TIMEOUT = 10000;

    private String servername;
    private Header authorizationHeader;

    private int timeout = DEFAULT_TIMEOUT;

    public AbstractApiProvider(String servername, Header authorizationHeader) {
        this.servername = servername;
        this.authorizationHeader = authorizationHeader;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    protected <T> T executeGet(Class<T> responseClass, String path) throws Exception {
        HttpGet httpGet = new HttpGet(getUrl(path));
        return execute(responseClass, httpGet);
    }

    protected HttpResponse executeGet(String path) throws Exception {
        HttpGet httpGet = new HttpGet(getUrl(path));
        return execute(httpGet);
    }

    private <T> T execute(Class<T> responseClass, HttpRequestBase httpRequestBase) throws Exception {
        T responseObj = null;

        HttpResponse httpResponse = execute(httpRequestBase);
        if (responseClass != null) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            String xmlString = reader.readLine();

            Serializer xmlSerializer = new Persister();
            responseObj = xmlSerializer.read(responseClass, xmlString);
        }
        return responseObj;
    }

    private HttpResponse execute(HttpRequestBase httpRequestBase) throws IOException, HttpException {
        HttpResponse httpResponse;

        httpRequestBase.addHeader(authorizationHeader);

        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpParams params = httpClient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, timeout);
        HttpConnectionParams.setSoTimeout(params, timeout);
        try {
            httpResponse = httpClient.execute(httpRequestBase);
            validateResponse(httpResponse);
        } finally {
            httpClient.getConnectionManager().shutdown();
        }
        return httpResponse;
    }

    private void validateResponse(HttpResponse response) throws HttpException {
        StatusLine status = response.getStatusLine();

        if (status.getStatusCode() != 200) {
            throw new HttpException(response);
        }
    }

    private String getUrl(String path) {
        return "http://" + servername + path;
    }

}
