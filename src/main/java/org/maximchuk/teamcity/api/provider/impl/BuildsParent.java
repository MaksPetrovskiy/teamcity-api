package org.maximchuk.teamcity.api.provider.impl;

import org.maximchuk.teamcity.api.entry.BuildEntry;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * @author Maxim L. Maximcuhk
 *         Date: 07.01.14
 */
@Root(strict = false)
public class BuildsParent {

    @ElementList(inline = true)
    private List<BuildEntry> builds;

    public List<BuildEntry> getBuilds() {
        return builds;
    }

    public void setBuilds(List<BuildEntry> builds) {
        this.builds = builds;
    }
}
