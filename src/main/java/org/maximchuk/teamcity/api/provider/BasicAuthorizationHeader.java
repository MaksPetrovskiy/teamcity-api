package org.maximchuk.teamcity.api.provider;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.ParseException;
import org.apache.http.message.BasicHeaderValueParser;

/**
 * @author Maxim L. Maximcuhk
 *         Date: 07.01.14
 */
public class BasicAuthorizationHeader implements Header {

    private static final String HEADER_NAME = "Authorization";

    private String value;

    public BasicAuthorizationHeader(String username, String password, IBase64Encoder encoder) {
        value = encoder.encode(
                new StringBuilder(username).append(":").append(password).toString().getBytes());
    }

    @Override
    public String getName() {
        return HEADER_NAME;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public HeaderElement[] getElements() throws ParseException {
        return BasicHeaderValueParser.parseElements(value, null);
    }
}
