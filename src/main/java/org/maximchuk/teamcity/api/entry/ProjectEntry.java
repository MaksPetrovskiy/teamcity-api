package org.maximchuk.teamcity.api.entry;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/**
 * @author Maxim L. Maximcuhk
 *         Date: 06.01.14
 */
@Root(name = "project", strict = false)
public class ProjectEntry implements Serializable {

    private static final long serialVersionUID = -7524514969455107085L;

    @Attribute
    private String id;

    @Attribute
    private String name;

    @Attribute
    private String href;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    @Override
    public String toString() {
        return name;
    }
}
